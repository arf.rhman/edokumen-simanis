<?php defined('BASEPATH') OR exit('No direct script access allowed');



class M_pelaksanaan extends CI_Model {



	public function __construct()

	{

		parent::__construct();

	}



	private static $table = 'tb_pelaksanaan';

	private static $pk = 'id';



	// public function is_exist($where)

	// {

	// 	return $this->db->where($where)->get(self::$table)->row_array();

	// }

	

	public function get_data_pelaksanaan_detail($where)

	{

		$query = $this

					->db

					->select('*')

					->from(self::$table)

					->join('users', 'users.u_id = tb_pelaksanaan.tujuan_kt_st', 'left')

					->where($where)

					->get();



		if ($query->num_rows() > 0) {

			return $query->row_array();

		} else {

			return NULL;

		}

	}



	// public function get_majors()

	// {

	// 	return $this->db->get('majors')->result_array();

	// }

	public function get_data_pelaksanaan()

	{
		$id = $this->session->userdata('u_id');

		$level = $this->session->userdata('u_level');

		$this->db->select('t.*,u1.u_fname AS nama_kt,u2.u_fname AS nama_opd');

		$this->db->join("users AS u1",'u1.u_id = t.tujuan_kt_st','left');

		$this->db->join("users AS u2",'u2.u_id = t.tujuan_opd_st','left');

		$this->db->where("(pembuat_st = '".$id."' AND status_st = 0 AND posisi_st = 0 ) OR 'Administrator' = '".$level."'");
		
		$this->db->where("s_is_deleted",'FALSE');

		$this->db->order_by("id","ASC");

		return $this->db->get(''.self::$table.' AS t')->result_array();

	}

	public function get_data_mytask($u_id)

	{
		$this->db->select('lap.id_pelaksanaan');

		$lap = $this->db->get('tb_laporan AS lap')->result_array();
		$rlap = array();
		foreach ($lap as $key => $value) {
			array_push($rlap, $value['id_pelaksanaan']);
		}
		$in_rlap = implode(',', $rlap);

		$level = $this->session->userdata('u_level');

		$this->db->select('t.*,u1.u_fname AS nama_kt,u2.u_fname AS nama_opd,u3.u_fname AS nama_pembuat_st');

		$this->db->join("users AS u1",'u1.u_id = t.tujuan_kt_st','left');

		$this->db->join("users AS u2",'u2.u_id = t.tujuan_opd_st','left');

		$this->db->join("users AS u3",'u3.u_id = t.pembuat_st','left');

		$this->db->where("(s_is_deleted = 'FALSE' AND posisi_st = '".$u_id."')");

		if(count($rlap)>0){
			$this->db->where("t.id NOT IN (".$in_rlap.")");
		}

		$this->db->order_by("id","ASC");

		return $this->db->get(''.self::$table.' AS t')->result_array();

	}

	public function get_data_pelaksanaan_detail_opd($id)

	{

		$this->db->where("id_pelaksanaan = ".$id);

		$this->db->order_by("index","ASC");

		return $this->db->get('tb_pelaksanaan_detail')->result_array();

	}

	

	public function get_ketua_tim()

	{

		return $this->db->get('majors')->result_array();

	}

	public function add($data)

	{

    	return $this->db->insert(self::$table, $data);

	}

	public function add_detail($data)

	{

    	return $this->db->insert('tb_pelaksanaan_detail', $data);

	}



	// public function do_import($data)

	// {

 //        return $this->db->insert(self::$table, $data);

	// }



	public function edit($data, $s_id)

	{

		return $this->db->set($data)->where(self::$pk, $s_id)->update(self::$table);

	}




	// public function status($data, $s_id)

	// {

	// 	return $this->db->set($data)->where(self::$pk, $s_id)->update(self::$table);

	// }



	// public function active($data, $s_id)

	// {

	// 	return $this->db->set($data)->where(self::$pk, $s_id)->update(self::$table);

	// }



	public function delete($data, $s_id)

	{

		return $this->db->set($data)->where(self::$pk, $s_id)->update(self::$table);

	}



	// public function restore($data, $s_id)

	// {

	// 	return $this->db->set($data)->where(self::$pk, $s_id)->update(self::$table);

	// }

}

