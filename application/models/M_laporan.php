<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_laporan extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	private static $table = 'tb_laporan';
	private static $pk = 'id';

	// public function is_exist($where)
	// {
	// 	return $this->db->where($where)->get(self::$table)->row_array();
	// }

	public function get_data_pelaksanaan_detail($where)
	{
		$query = $this
					->db
					->select('*')
					->from(self::$table)
					->join('users', 'users.u_id = tb_pelaksanaan.tujuan_kt_st', 'left')
					->where($where)
					->get();

		if ($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return NULL;
		}
	}

	// public function get_majors()
	// {
	// 	return $this->db->get('majors')->result_array();
	// }
	
	public function get_data_laporan()
	{
		$id = $this->session->userdata('u_id');
		$level = $this->session->userdata('u_level');

		$this->db->select('t.*,DATE_FORMAT(p.tanggal_upload_st,"%Y-%m-%d") AS tanggal_st,datediff(t.tanggal_upload_laporan,DATE_FORMAT(p.tanggal_upload_st,"%Y-%m-%d")) AS durasi');
		$this->db->join("tb_pelaksanaan AS p",'p.id = t.id_pelaksanaan','left');
		$this->db->where("(tujuan_laporan = '".$id."' OR pembuat_laporan = '".$id."' OR tujuan_laporan_opd = '".$id."' OR 'Administrator' = '".$level."')");
		$this->db->where("s_is_deleted",'FALSE');
		
		$this->db->order_by("id","ASC");
		return $this->db->get(''.self::$table.' AS t')->result_array();
	}
	
	public function get_ketua_tim()
	{
		return $this->db->get('majors')->result_array();
	}
	public function add($data)
	{
    	return $this->db->insert(self::$table, $data);
	}
	public function add_detail($data)
	{
    	return $this->db->insert('tb_pelaksanaan_detail', $data);
	}

	// public function do_import($data)
	// {
 //        return $this->db->insert(self::$table, $data);
	// }

	public function edit($data, $s_id)
	{
		return $this->db->set($data)->where(self::$pk, $s_id)->update(self::$table);
	}

	// public function status($data, $s_id)
	// {
	// 	return $this->db->set($data)->where(self::$pk, $s_id)->update(self::$table);
	// }

	// public function active($data, $s_id)
	// {
	// 	return $this->db->set($data)->where(self::$pk, $s_id)->update(self::$table);
	// }

	public function delete($data, $s_id)
	{
		return $this->db->set($data)->where(self::$pk, $s_id)->update(self::$table);
	}

	// public function restore($data, $s_id)
	// {
	// 	return $this->db->set($data)->where(self::$pk, $s_id)->update(self::$table);
	// }
}
