<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_dashboards extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	private static $table2 = 'tb_pelaksanaan';

	public function total($search='')
	{
		$id = $this->session->userdata('u_id');
		$level = $this->session->userdata('u_level');
		if($search!=''){
			$this->db->where('tujuan_opd_st',$search);
		}
		if($level=='Administrator'){
			return $this->db->where("s_is_deleted = 'FALSE'")->get(self::$table2)->result();
		}else{
			return $this->db->where("s_is_deleted = 'FALSE'  AND (pembuat_st = '".$id."' || tujuan_kt_st = '".$id."' || tujuan_opd_st = '".$id."')")->get(self::$table2)->result();
		}
	}
	public function pembinaan($search='')
	{
		$id = $this->session->userdata('u_id');
		$level = $this->session->userdata('u_level');
		if($search!=''){
			$this->db->where('tujuan_opd_st',$search);
		}
		if($level=='Administrator'){
			return $this->db->where("s_is_deleted = 'FALSE' AND jenis_st = 1")->get(self::$table2)->result();
		}else{
		return $this->db->where("s_is_deleted = 'FALSE' AND jenis_st = 1 AND (pembuat_st = '".$id."' || tujuan_kt_st = '".$id."' || tujuan_opd_st = '".$id."')")->get(self::$table2)->result();
		}
	}
	public function pengawasan($search='')
	{
		$id = $this->session->userdata('u_id');
		$level = $this->session->userdata('u_level');
		if($search!=''){
			$this->db->where('tujuan_opd_st',$search);
		}
		if($level=='Administrator'){
			return $this->db->where("s_is_deleted = 'FALSE' AND jenis_st = 2 ")->get(self::$table2)->result();
		}else{
		return $this->db->where("s_is_deleted = 'FALSE' AND jenis_st = 2 AND (pembuat_st = '".$id."' || tujuan_kt_st = '".$id."' || tujuan_opd_st = '".$id."')")->get(self::$table2)->result();
		}
	}
	public function get_opd($u_id)
	{
		if($u_id=='all'){
			$query = $this->db
					->select('tujuan_opd_st,u_instansi')
					->from(self::$table2)
					->join('users', 'users.u_id = tb_pelaksanaan.tujuan_opd_st')
					->where("tujuan_opd_st IS NOT NULL")
					->where("s_is_deleted = 'FALSE'")
					->group_by('tujuan_opd_st')
					->get();

		}else{
			$query = $this->db
					->select('tujuan_opd_st,u_instansi')
					->from(self::$table2)
					->join('users', 'users.u_id = tb_pelaksanaan.tujuan_opd_st')
					->where("(pembuat_st = '".$u_id."' OR tujuan_kt_st = '".$u_id."')")
					->where("tujuan_opd_st IS NOT NULL")
					->where("s_is_deleted = 'FALSE'")
					->group_by('tujuan_opd_st')
					->get();
		}

		if ($query->num_rows() > 0) {
				return $query->result();
			} else {
				return NULL;
			}
	}

	public function data_export($search='')
	{
		$id = $this->session->userdata('u_id');
		$level = $this->session->userdata('u_level');

		$this->db->select('t.*,p.*,DATE_FORMAT(p.tanggal_upload_st,"%Y-%m-%d") AS tanggal_st,datediff(t.tanggal_upload_laporan,DATE_FORMAT(p.tanggal_upload_st,"%Y-%m-%d")) AS durasi,u.u_name');
		$this->db->join("tb_laporan AS t",'p.id = t.id_pelaksanaan','left');
		$this->db->join("users AS u",'u.u_id = p.tujuan_kt_st','left');
		$this->db->where("(tujuan_kt_st = '".$id."' OR pembuat_st = '".$id."' OR 'Administrator' = '".$level."')");
		if($search!="" && $search!="all"){
			$this->db->where("tujuan_opd_st = '".$search."'");
		}
		$this->db->where("s_is_deleted = 'FALSE'");

		return $this->db->get('tb_pelaksanaan AS p')->result();
	}

	public function belum($wb)
	{
		return $this->db->where($wb)->get(self::$table2)->result();
	}

	public function kurang($wk)
	{
		return $this->db->where($wk)->get(self::$table2)->result();
	}

	public function lengkap($wl)
	{
		return $this->db->where($wl)->get(self::$table2)->result();
	}

	public function belumx($wbx)
	{
		return $this->db->where($wbx)->get(self::$table2)->result();
	}

	public function kurangx($wk)
	{
		return $this->db->where($wk)->get(self::$table2)->result();
	}

	public function lengkapx($wl)
	{
		return $this->db->where($wl)->get(self::$table2)->result();
	}
}
