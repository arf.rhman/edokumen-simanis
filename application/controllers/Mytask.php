<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Mytask extends CI_Controller {



 	public function __construct()

    {

        parent::__construct();

        $this->load->model('m_pelaksanaan');

        $this->load->model('m_users');

        // $this->auth->restrict();

    }



    private static $title = "E &minus; DOKUMEN | SIMANIS BONTANG";

    private static $table = 'tb_pelaksanaan';

    private static $primaryKey = 'id';



    public function index()

	{
        $data['title'] = "Data ".self::$title;
        if($this->session->userdata['u_level']=='OPD'){

            $data['content'] = "dashboard/mytask-opd";

        }else{

            $data['content'] = "dashboard/mytask";

        }

        $data['label_posisi'] = array('0'=>'danger','2'=>'success','3'=>'primary');

        $data['posisi'] = array('0'=>'Ketua Tim','2'=>'OPD','3'=>'Proses Laporan KT');

        $data['pelaksanaan'] = $this->m_pelaksanaan->get_data_mytask($this->session->userdata['u_id']);

		$this->load->view('dashboard/index', $data);

	}



    public function share_opd($id){

        $this->load->helper(['form', 'string', 'notification']);

        $where = "id = '$id'";

        $data['pelaksanaan'] = $this->m_pelaksanaan->get_data_pelaksanaan_detail($where);



        if($this->input->post()) {



            $data = [

            'tujuan_opd_st' => $this->input->post('tujuan_opd_st', TRUE),

            'keterangan' => $this->input->post('keterangan', TRUE),

            'status_posisi_st' => 2,

            'posisi_st' => $this->input->post('tujuan_opd_st', TRUE),

            'status_st' => 0

            ];



            $this->m_pelaksanaan->edit($data,$id);



            $this->session->set_flashdata('alert', success('Data Surat Tugas Berhasil Dishare'));

            $data['title'] = "Data ".self::$title;

            $data['content'] = "dashboard/mytask";

            redirect('mytask');

        } else {

         $this->load->helper(['form', 'notification']);

         $where = "u_level = 'OPD'";

         $data['opd'] = $this->m_users->get_ketua_tim($where);

         $data['title'] = "Kirim OPD ".self::$title;

         $data['form_title'] = "Share Ke OPD";

         $data['s_name'] = "";

         $data['action'] = site_url(uri_string());

         $data['content'] = 'dashboard/share-opd-form';

         $this->load->view('dashboard/index', $data);

        }

    }



    public function proses_st($id){

        $this->load->helper(['form', 'string', 'notification']);

        $where = "id = '$id'";

        $data['pelaksanaan'] = $this->m_pelaksanaan->get_data_pelaksanaan_detail($where);



        if($this->input->post()) {

            $this->load->library('upload');

            for ($i=1; $i < 6; $i++) { 

                $nm_file = "file_pendukung".$i;

                $nm_ket = "keterangan".$i;

                $file_pendukung = $_FILES[$nm_file]['name'];

                $ket = $this->input->post($nm_ket);



                if (!empty($file_pendukung)) {

                    $s_id = random_string('alnum', 10);

                    $config['upload_path'] = './uploads/pendukung/';

                    $config['allowed_types'] = 'pdf|doc|docx|jpg|png|jpeg|rar|zip|ppt|pps';

                    $config['file_ext_tolower'] = TRUE;

                    $config['max_size'] = '10240';

                    $config['overwrite'] = TRUE;

                    $x = explode(".", $file_pendukung);

                    $ext = strtolower(end($x));

                    $config['file_name'] = $s_id.'-'.date('Y').'.'.$ext;

                    $files = $config['file_name'];

                    $this->upload->initialize($config);

                    $this->upload->do_upload($nm_file);



                $data_detail = [

                'id_pelaksanaan' => $id,

                'keterangan' => (!empty($ket)) ? $ket : NULL,

                'file_pendukung' => $files,

                'tanggal_upload_pendukung' => date('Y-m-d'),

                'index' => $i

                ];

                $this->m_pelaksanaan->add_detail($data_detail);

                }

            }

            $data_pel = [

            'keterangan_opd' => $this->input->post('keterangan_opd', TRUE),

            'status_posisi_st' => 3,

            'posisi_st' => $data['pelaksanaan']['tujuan_kt_st'],

            'status_st' => 0

            ];

            $this->m_pelaksanaan->edit($data_pel,$id);



            $this->session->set_flashdata('alert', success('Proses Upload Data Pendukung Sukses'));

            $data['title'] = "Data ".self::$title;

            $data['content'] = "dashboard/mytask";

            redirect('mytask');

        } else {

             $this->load->helper(['form', 'notification']);

             $data['title'] = "Proses ST ".self::$title;

             $data['form_title'] = "Proses Surat Tugas";

             $data['s_name'] = "";

             $data['action'] = site_url(uri_string());

             $data['content'] = 'dashboard/proses-opd-form';

             $this->load->view('dashboard/index', $data);

        }

    }



    public function detail($id){

        $this->load->helper(['form', 'string', 'notification']);

        $where = "id = '$id'";

        $data['pelaksanaan'] = $this->m_pelaksanaan->get_data_pelaksanaan_detail($where);

        $data['det_pelaksanaan'] = $this->m_pelaksanaan->get_data_pelaksanaan_detail_opd($id);



        if($this->input->post()) {

            $this->load->library('upload');

                $file_pendukung = $_FILES['file_lap']['name'];

                $ket = $this->input->post('keterangan_laporan');



                if (!empty($file_pendukung)) {

                    $s_id = random_string('alnum', 10);

                    $config['upload_path'] = './uploads/laporan/';

                    $config['allowed_types'] = 'pdf|doc|docx|jpg|png|jpeg';

                    $config['file_ext_tolower'] = TRUE;

                    $config['max_size'] = '10240';

                    $config['overwrite'] = TRUE;

                    $x = explode(".", $file_pendukung);

                    $ext = strtolower(end($x));

                    $config['file_name'] = $s_id.'-'.date('Y').'.'.$ext;

                    $files = $config['file_name'];

                    $this->upload->initialize($config);

                    $this->upload->do_upload($file_pendukung);



                $data_detail = [

                'id_pelaksanaan' => $id,

                'file_laporan' => $files,

                'keterangan_laporan' => (!empty($ket)) ? $ket : NULL,

                'tanggal_upload_laporan' => date('Y-m-d'),

                'tujuan_laporan' =>  $data['pelaksanaan']['pembuat_st'],

                ];

                $this->m_laporan->add($data_detail);

                }

            



            $this->session->set_flashdata('alert', success('Proses Input Laporan Sukses'));

            $data['title'] = "Data ".self::$title;

            $data['content'] = "dashboard/laporan";

            redirect('mytask');

        } else {

             $this->load->helper(['form', 'notification']);

             $data['title'] = "Detail ST ".self::$title;

             $data['form_title'] = "Detail Surat Tugas";

             $data['s_name'] = "";

             $data['action'] = site_url(uri_string());

             $data['content'] = 'dashboard/detail_pelaksanaan';

             $this->load->view('dashboard/index', $data);

        }

    }



    public function get_data(){

        if (!$this->input->is_ajax_request()) {

            exit('No direct script access allowed');

        } else {

            // $this->load->library('datatables_ssp');

            // $columns = array(

            //    array('db' => 'nomor_st', 'dt' => 'nomor_st'),

            //    array('db' => 'tanggal_st', 'dt' => 'tanggal_st'),

            //    array('db' => 'uraian_st', 'dt' => 'uraian_st'),

            //    array(

            //       'db' => 'jenis_st',

            //       'dt' => 'jenis_st',

            //       'formatter' => function($jenis_st) {

            //         if($jenis_st==1){

            //           return '<label class="label label-danger">Pengawasan</label>';

            //       }else{

            //           return '<label class="label label-primary">Pembinaan</label>';

            //       }

            //   }

            //   ),

            //    array(

            //       'db' => 'tanggal_upload_st',

            //       'dt' => 'tanggal_upload_st',

            //       ),

            //     array(

            //       'db' => 'u_fname',

            //       'dt' => 'tujuan_kt_st',

            //       ),

            //      array(

            //       'db' => 'tujuan_kt_st',

            //       'dt' => 'tujuan_opd_st',

            //       ),

            //     array(

            //       'db' => 'id',

            //       'dt' => 'tindakan',

            //       'formatter' => function($id) {

            //         return '<a class="btn btn-primary btn-sm mb"><span class="glyphicon glyphicon-share" aria-hidden="true"></span></a><a class="btn btn-success btn-sm mb" href="#" target="_blank" title="Download"><span class="glyphicon glyphicon-download" aria-hidden="true"></span></a><a class="btn btn-warning btn-sm mb" href="#"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a><a class="btn btn-danger btn-sm mb" href="#"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>';

            //       }

            //       ),

            // );



            // $sql_details = [

            //     'user' => $this->db->username,

            //     'pass' => $this->db->password,

            //     'db' => $this->db->database,

            //     'host' => $this->db->hostname

            // ];



            // $qjoin = "FROM ".self::$table." AS t LEFT JOIN users AS u1 ON u1.u_id = t.tujuan_kt_st";



            // echo json_encode(

            //     Datatables_ssp::complex($_GET, $sql_details, self::$table, self::$primaryKey, $columns, NULL, "status_st = '0' AND posisi_st='0'",$qjoin)

            // );



        }

    }





    public function add()

    {

        $this->load->helper(['form', 'string', 'notification']);



        if ($this->input->post()) {

          $array_jenis_dokumen = array(1=>"Pembinaan",2=>"Pengawasan");

          $s_id = random_string('alnum', 10);

          $jenis_dokumen = $array_jenis_dokumen[$this->input->post('jenis_st', TRUE)];



          $file_st = $_FILES['file_st']['name'];



          $this->load->library('upload');



          if (!empty($file_st)) {

            $config['upload_path'] = './uploads/ST/'.$jenis_dokumen.'/';

            $config['allowed_types'] = 'pdf|doc|docx|jpg|png|jpeg';

            $config['file_ext_tolower'] = TRUE;

            $config['max_size'] = '10240';

            $config['overwrite'] = TRUE;

            $x = explode(".", $file_st);

            $ext = strtolower(end($x));

            $config['file_name'] = $file_st.'-'.$s_id.'-'.date('Y').'.'.$ext;

            $files = $config['file_name'];

            $this->upload->initialize($config);

            $this->upload->do_upload('file_st');

                }



                $data = [

                'nomor_st' => $this->input->post('nomor_st', TRUE),

                'uraian_st' => $this->input->post('uraian_st', TRUE),

                'tanggal_st' => $this->input->post('tanggal_st', TRUE),

                'tujuan_kt_st' => $this->input->post('tujuan_kt_st', TRUE),

                'jenis_st' => $this->input->post('jenis_st', TRUE),

                'file_st' => (!empty($files)) ? $files : NULL,

                'file_st' => (!empty($files)) ? $files : NULL,

                'tanggal_upload_st' => date('Y-m-d H:i:s'),

                'posisi_st' => 0,

                'status_st' => 0

                ];



                $this->m_pelaksanaan->add($data);



                $this->session->set_flashdata('alert', success('Data Surat Tugas Berhasil Ditambahkan'));

                $data['title'] = "Data ".self::$title;

                $data['content'] = "dashboard/pelaksanaan";

                redirect('pelaksanaan');



        } else {

         $this->load->helper(['form', 'notification']);

         $where = "u_level = 'Ketua Tim'";

         $data['ketua_tim'] = $this->m_users->get_ketua_tim($where);

         $data['title'] = "Tambah ".self::$title;

         $data['form_title'] = "Tambah Data ST";

         $data['s_name'] = "";

         $data['action'] = site_url(uri_string());

         $data['content'] = 'dashboard/pelaksanaan-form';

         $this->load->view('dashboard/index', $data);

        }

    }


    public function edit()

    {

        $this->load->helper(['form', 'string', 'notification']);

        $s_id = $this->uri->segment(3);

        $where = "id = '$s_id'";

        $data['pelaksanaan'] = $this->m_pelaksanaan->get_data_pelaksanaan_detail($where);

        $array_jenis_dokumen = array(1=>"Pembinaan",2=>"Pengawasan");



        if ($this->input->post()) {

          $jenis_dokumen = $array_jenis_dokumen[$this->input->post('jenis_st', TRUE)];





          $this->load->library('upload');



          $s_idd = random_string('alnum', 10);

          $file_st = $_FILES['file_st']['name'];

          if (!empty($file_st)) {

            $config['upload_path'] = './uploads/ST/'.$jenis_dokumen.'/';

            $config['allowed_types'] = 'pdf|doc|docx|jpg|png|jpeg';

            $config['file_ext_tolower'] = TRUE;

            $config['max_size'] = '10240';

            $config['overwrite'] = TRUE;

            $x = explode(".", $file_st);

            $ext = strtolower(end($x));

            $config['file_name'] = $file_st.'-'.$s_idd.'-'.date('Y').'.'.$ext;

            $files = $config['file_name'];

            $this->upload->initialize($config);

            $this->upload->do_upload('file_st');

        }

        $data = [

        'nomor_st' => $this->input->post('nomor_st', TRUE),

        'uraian_st' => $this->input->post('uraian_st', TRUE),

        'tanggal_st' => $this->input->post('tanggal_st', TRUE),

        'tujuan_kt_st' => $this->input->post('tujuan_kt_st', TRUE),

        'jenis_st' => $this->input->post('jenis_st', TRUE),

        'file_st' => (!empty($files)) ? $files : NULL,

        'tanggal_upload_st' => date('Y-m-d H:i:s'),

        'posisi_st' => 0,

        'status_st' => 0

        ];



        $this->m_pelaksanaan->edit($data, $s_id);

        $this->session->set_flashdata('alert', success('Data Surat Tugas berhasil diperbarui.'));

        $data['title'] = "Data ".self::$title;

        $data['content'] = "dashboard/pelaksanaan";

        redirect(site_url('pelaksanaan'));



    } else {



       $where = "u_level = 'Ketua Tim'";

       $data['ketua_tim'] = $this->m_users->get_ketua_tim($where);

       $data['title'] = "Edit ".self::$title;

       $data['form_title'] = "Edit Data ST ". $data['pelaksanaan']['nomor_st'];

       $data['action'] = site_url(uri_string());

       $data['content'] = 'dashboard/pelaksanaan-form';

       if (!$s_id) {

        redirect(site_url('pelaksanaan'));

    } else {

        $this->load->view('dashboard/index', $data);

    }

}



}



    public function delete($id)

    {

        $this->load->helper('notification');



        $data = [

            's_deleted_at' => date('Y-m-d H:i:s'),

            's_deleted_by' => $this->session->userdata['u_id'],

            's_is_deleted' => TRUE

        ];



        $this->m_pelaksanaan->delete($data, $id);

        $this->session->set_flashdata('alert', success('Data Surat Tugas berhasil dihapus.'));

        $data['title'] = "Data ".self::$title;

        $data['content'] = "dashboard/pelaksanaan";

        $this->load->view('dashboard/index', $data);

        redirect(site_url('pelaksanaan'));

    }

}

