<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

 	public function __construct()
    {
        parent::__construct();
        $this->load->model('m_dashboards');
        $this->auth->restrict();
}


	public function index()
	{
        
        $data['title'] = "E &minus; DOKUMEN | SIMANIS BONTANG";
        $data['content'] = 'dashboard/home';
        $search = '';
        if($_POST){
            $search = $this->input->post('filteropd');
        }
        
        if($this->session->userdata['u_level'] == 'Ketua Tim' || $this->session->userdata['u_level'] == 'Irban'){
            $data['opd'] = $this->m_dashboards->get_opd($this->session->userdata['u_id']);
        }elseif($this->session->userdata['u_level'] == 'Administrator'){
            $data['opd'] = $this->m_dashboards->get_opd('all');
        }


        $data['pembinaan'] = $this->m_dashboards->pembinaan($search);
        $data['pengawasan'] = $this->m_dashboards->pengawasan($search);
        $data['total'] = $this->m_dashboards->total($search);
		$this->load->view('dashboard/index', $data);
	}

    public function export(){
    include APPPATH.'libraries/PHPExcel.php';
    $search = $_GET['q'];
    // Panggil class PHPExcel nya
    $excel = new PHPExcel();
    // Settingan awal fill excel
    $excel->getProperties()->setCreator('simanis')
                 ->setLastModifiedBy('simanis')
                 ->setTitle("Data Pelaksanaan")
                 ->setSubject("Data Pelaksanaan")
                 ->setDescription("Laporan Semua Data Pelaksanaan")
                 ->setKeywords("Data Pelaksanaan");
    // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
    $style_col = array(
      'font' => array('bold' => true), // Set font nya jadi bold
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
    $style_row = array(
      'alignment' => array(
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
      ),
      'borders' => array(
        'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
        'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
        'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
        'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
      )
    );
    $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA PELAKSANAAN"); // Set kolom A1 dengan tulisan "DATA INVOICE"
    $excel->getActiveSheet()->mergeCells('A1:F1'); // Set Merge Cell pada kolom A1 sampai E1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
    $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
    $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO."); 
    $excel->setActiveSheetIndex(0)->setCellValue('B3', "ST / TANGGAL ST"); 
    $excel->setActiveSheetIndex(0)->setCellValue('C3', "URAIAN");
    $excel->setActiveSheetIndex(0)->setCellValue('D3', "KETUA TIM");
    $excel->setActiveSheetIndex(0)->setCellValue('E3', "TGL UPLOAD ST");
    $excel->setActiveSheetIndex(0)->setCellValue('F3', "NO. LAPORAN");
    $excel->setActiveSheetIndex(0)->setCellValue('G3', "TGL LAPORAN");
    $excel->setActiveSheetIndex(0)->setCellValue('H3', "JUDUL LAPORAN");
    $excel->setActiveSheetIndex(0)->setCellValue('I3', "TGL UPLOAD LAPORAN");
    $excel->setActiveSheetIndex(0)->setCellValue('J3', "DURASI");

    $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
    $excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
    $data_lap = $this->m_dashboards->data_export($search);
    // print_r($data_lap); die();
    $no = 1; // Untuk penomoran tabel, di awal set dengan 1
    $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
    foreach($data_lap as $data){
      $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
      $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->nomor_st.' / '.$data->tanggal_laporan);
      $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->uraian_st);
      $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->u_name);
      $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, date("d/m/Y H:i:s", strtotime($data->tanggal_upload_st)));
      $excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $data->nomor_laporan);
      $excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $data->tanggal_laporan);
      $excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $data->judul_laporan);
      $excel->setActiveSheetIndex(0)->setCellValue('I'.$numrow, $data->tanggal_upload_laporan);
      $excel->setActiveSheetIndex(0)->setCellValue('J'.$numrow, $data->durasi+1);

      
    //   // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
      $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('I'.$numrow)->applyFromArray($style_row);
      $excel->getActiveSheet()->getStyle('J'.$numrow)->applyFromArray($style_row);
      
      $no++; // Tambah 1 setiap kali looping
      $numrow++; // Tambah 1 setiap kali looping
    }
    // Set width kolom
    $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
    $excel->getActiveSheet()->getColumnDimension('B')->setWidth(25); // Set width kolom B
    $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
    $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
    $excel->getActiveSheet()->getColumnDimension('E')->setWidth(30); // Set width kolom E
    $excel->getActiveSheet()->getColumnDimension('F')->setWidth(15); // Set width kolom E
    $excel->getActiveSheet()->getColumnDimension('G')->setWidth(15); // Set width kolom E
    $excel->getActiveSheet()->getColumnDimension('H')->setWidth(15); // Set width kolom E
    $excel->getActiveSheet()->getColumnDimension('I')->setWidth(15); // Set width kolom E
    $excel->getActiveSheet()->getColumnDimension('J')->setWidth(15); // Set width kolom E
    
    // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
    $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
    // Set orientasi kertas jadi LANDSCAPE
    $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    // Set judul file excel nya
    $excel->getActiveSheet(0)->setTitle("Data Simanis");
    $excel->setActiveSheetIndex(0);
    // Proses file excel
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment; filename="Data Simanis.xlsx"'); // Set nama file excel nya
    header('Cache-Control: max-age=0');
    $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
    $write->save('php://output');
  }

}
