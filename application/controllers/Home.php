<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

 	public function __construct()
    {
        parent::__construct();
        $this->load->model('m_homes');
        $this->auth->auth();
    }

	public function index()
	{
        $this->load->helper('form');
        $data['title'] = "E &minus; DOKUMEN | SIMANIS BONTANG";
        $data['action'] = "";
        $data['content'] = 'home';
		$this->load->view('index', $data);
	}

    public function result()
    {
        $keyword = $this->input->get('keyword', TRUE);

        if (is_null($keyword)) {
            $this->load->helper('form');
        } else {
            $this->load->model('m_homes');
            $this->load->helper('form');
            $data['result'] = $this->m_homes->get_result($keyword);
            $data['keyword'] = $keyword;
            $this->load->view('result', $data);
        }
    }
}
