/*
 Navicat Premium Data Transfer

 Source Server         : mysql local
 Source Server Type    : MySQL
 Source Server Version : 100134
 Source Host           : localhost:3306
 Source Schema         : db_arsip

 Target Server Type    : MySQL
 Target Server Version : 100134
 File Encoding         : 65001

 Date: 07/11/2019 23:38:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ci_sessions
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions`  (
  `id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL,
  INDEX `ci_sessions_timestamp`(`timestamp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('8eke9bd4or0p8e8gh3f11h8kkgjn6fpl', '::1', 1573116479, 0x5F5F63695F6C6173745F726567656E65726174657C693A313537333131363434343B755F69647C733A353A223041317972223B755F6E616D657C733A373A2268617279616469223B755F666E616D657C733A373A2248617279616469223B755F6C6576656C7C733A393A224B657475612054696D223B69735F6C6F676765645F696E7C623A313B);
INSERT INTO `ci_sessions` VALUES ('7rcprhs0f034h39bpnod2n446ij41230', '::1', 1573116497, 0x5F5F63695F6C6173745F726567656E65726174657C693A313537333131363438393B755F69647C733A353A227843326133223B755F6E616D657C733A373A226D61727468656E223B755F666E616D657C733A32343A224D61727468656E204D696E6767752C2053452C204D2E5369223B755F6C6576656C7C733A333A224F5044223B69735F6C6F676765645F696E7C623A313B);
INSERT INTO `ci_sessions` VALUES ('b2rclhako614aaa28o8rdju6bojd7qst', '::1', 1573121448, 0x5F5F63695F6C6173745F726567656E65726174657C693A313537333132313136343B);
INSERT INTO `ci_sessions` VALUES ('si41i5a7vmo75uod02ptbppdth673vaa', '::1', 1573121366, 0x5F5F63695F6C6173745F726567656E65726174657C693A313537333132313231363B);
INSERT INTO `ci_sessions` VALUES ('hoj1tjrlpk2rqib36rp0evgl2no16n0g', '::1', 1573138848, 0x5F5F63695F6C6173745F726567656E65726174657C693A313537333133383834383B);
INSERT INTO `ci_sessions` VALUES ('llig3b83tbu4i1lurn97bas0edoa2j8a', '::1', 1573144247, 0x5F5F63695F6C6173745F726567656E65726174657C693A313537333134343036313B755F69647C733A353A223041317972223B755F6E616D657C733A373A2268617279616469223B755F666E616D657C733A373A2248617279616469223B755F6C6576656C7C733A393A224B657475612054696D223B69735F6C6F676765645F696E7C623A313B);
INSERT INTO `ci_sessions` VALUES ('u9ibncu4hvg57ad1tr1pjufli3bk5bpj', '::1', 1573144570, 0x5F5F63695F6C6173745F726567656E65726174657C693A313537333134343535373B755F69647C733A353A22304E697949223B755F6E616D657C733A363A22697262616E31223B755F666E616D657C733A32323A22456B6F2041726973616E64692C2053542C204D2E5369223B755F6C6576656C7C733A353A22497262616E223B69735F6C6F676765645F696E7C623A313B);

-- ----------------------------
-- Table structure for tb_laporan
-- ----------------------------
DROP TABLE IF EXISTS `tb_laporan`;
CREATE TABLE `tb_laporan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelaksanaan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nomor_st` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file_laporan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `link_gdrive` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nomor_laporan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `judul_laporan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan_laporan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal_laporan` date NULL DEFAULT NULL,
  `tanggal_upload_laporan` date NULL DEFAULT NULL,
  `tujuan_laporan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pembuat_laporan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_laporan
-- ----------------------------
INSERT INTO `tb_laporan` VALUES (1, '8', 'ST-001', '3FVNZMgmce-2019.pdf', '', 'LAP-001', 'aa', '11', '2019-11-07', '2019-11-07', '0NiyI', '0A1yr');

-- ----------------------------
-- Table structure for tb_pelaksanaan
-- ----------------------------
DROP TABLE IF EXISTS `tb_pelaksanaan`;
CREATE TABLE `tb_pelaksanaan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pembuat_st` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tujuan_kt_st` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tujuan_opd_st` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nomor_st` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal_st` date NULL DEFAULT NULL,
  `uraian_st` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jenis_st` smallint(1) NOT NULL COMMENT '1 = pembinaan,2=pengawasan',
  `tanggal_upload_st` datetime(0) NULL DEFAULT NULL,
  `file_st` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `keterangan_opd` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `posisi_st` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status_posisi_st` smallint(1) NULL DEFAULT NULL COMMENT '0 = baru dibuat, 1 = awal ketua tim, 2 = opd , 3 = kedua ketua tim',
  `status_st` smallint(1) NULL DEFAULT NULL COMMENT '0 = sedang proses, 1 = selesai, 2= dihapus',
  `s_deleted_at` timestamp(0) NULL DEFAULT NULL,
  `s_deleted_by` char(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `s_is_deleted` enum('TRUE','FALSE') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'FALSE',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_pelaksanaan
-- ----------------------------
INSERT INTO `tb_pelaksanaan` VALUES (8, '0NiyI', '0A1yr', 'xC2a3', 'ST-001', '2019-01-01', 'ST untuk pembinaan dinas pariwisata', 1, '2019-11-07 23:07:06', 'A0aJhoyXx8-2019.pdf', 'Tolong di upload data terkait pembinaan', 'Sudah lengkap', '0A1yr', 3, 0, NULL, NULL, 'FALSE');

-- ----------------------------
-- Table structure for tb_pelaksanaan_detail
-- ----------------------------
DROP TABLE IF EXISTS `tb_pelaksanaan_detail`;
CREATE TABLE `tb_pelaksanaan_detail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelaksanaan` int(11) NULL DEFAULT NULL,
  `file_pendukung` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `tanggal_upload_pendukung` date NULL DEFAULT NULL,
  `index` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tb_pelaksanaan_detail
-- ----------------------------
INSERT INTO `tb_pelaksanaan_detail` VALUES (20, 8, 'O5gfjVaM9I-2019.pdf', 'Pendukung 1', '2019-11-07', 1);
INSERT INTO `tb_pelaksanaan_detail` VALUES (21, 8, 'swQZ2G6SIl-2019.jpeg', 'Pendukun 2', '2019-11-07', 2);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `u_id` char(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `u_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `u_pass` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `u_nip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `u_fname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `u_level` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'User Biasa',
  `u_created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `u_updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  `u_created_by` char(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `u_updated_by` char(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `u_password_updated_at` timestamp(0) NULL DEFAULT NULL,
  `u_last_logged_in` timestamp(0) NULL DEFAULT NULL,
  `u_ip_address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `u_is_active` enum('Aktif','Tidak Aktif') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'Aktif',
  PRIMARY KEY (`u_id`) USING BTREE,
  UNIQUE INDEX `uname`(`u_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('0A1yr', 'haryadi', '$2y$10$6JGwrxsrZUDwmM5cvp.soOIH1b3DdM4axRUPbAfzIGXYlgJzAcQ82', '196501012003101001', 'Haryadi', 'Ketua Tim', '2019-10-25 16:44:23', '2019-11-07 23:22:02', 'f5VWt', NULL, NULL, '2019-11-07 23:22:02', '::1', 'Aktif');
INSERT INTO `users` VALUES ('0NiyI', 'irban1', '$2y$10$Bd9THgNFSnwDv5tCXTEbmOz1AyvuiV2xUoZki1wrWlWoUtOynFwUq', '197505152003151011', 'Eko Arisandi, ST, M.Si', 'Irban', '2019-10-25 16:15:47', '2019-11-07 22:59:58', 'f5VWt', NULL, NULL, '2019-11-07 22:59:58', '::1', 'Aktif');
INSERT INTO `users` VALUES ('f5VWt', 'admin', '$2y$10$KogWYMDMBneVHP1weiI/oevrfist4VY8SspCO1NTLczPsJW4eyKkK', '-', 'Admin', 'Administrator', '2019-10-23 06:32:41', '2019-11-07 22:12:53', 'xB3gG', NULL, NULL, '2019-11-07 22:12:53', '::1', 'Aktif');
INSERT INTO `users` VALUES ('IDcO7', 'irban2', '$2y$10$D0l1psHS0mXCCZY8su4nDuF8dEdlRHEugiMo5jGjTRlQ6abaF7yYK', '196502061995031001', 'Ir. Murlan', 'Irban', '2019-10-25 16:34:43', '2019-10-25 16:40:46', 'f5VWt', NULL, NULL, NULL, NULL, 'Aktif');
INSERT INTO `users` VALUES ('xC2a3', 'marthen', '$2y$10$7wA11phgWv3xE5R2v155oODEfH.BIW30BK7V0L4vr1WxtmJOrOxk2', '197203212001121001', 'Marthen Minggu, SE, M.Si', 'OPD', '2019-10-25 16:45:57', '2019-11-07 23:08:38', 'f5VWt', NULL, NULL, '2019-11-07 23:08:38', '::1', 'Aktif');

SET FOREIGN_KEY_CHECKS = 1;
