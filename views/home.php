<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper">
    <div class="header">
        <div class="logo">
            <img src="<?=site_url('assets/img/logo.png')?>" alt="Logo">
        </div>
        <h1>SIMANIS BONTANG</h1>
        <h3>Sistem Manajemen Arsip Inspektorat Bontang</h3>
    </div>
    <div class="center" style="width: 55%;margin: auto;">
       <?=form_open('login', 'id="login"')?>
       <div id='success' class='alert alert-success' role='alert'>
        <span class='glyphicon glyphicon-ok' aria-hidden='true'></span>
        <strong>Login sukses ...</strong>
    </div>
    <div id='failed' class='alert alert-error' role='alert'>
        <span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>
        <strong>Login gagal !</strong>
    </div>
    <div class="form-group">
        <input type="text" name="u_name" class="form-control md" id="username" value="<?=set_value('u_name')?>" placeholder="Username" minlength="5" maxlength="20">
    </div>
    <div class="form-group input-group">
        <input type="password" name="u_pass" class="form-control md"  id="password" value="<?=set_value('u_pass')?>" placeholder="Password" minlength="5" pass-shown="false">
        <span class="input-group-btn">
            <button class="btn btn-default md sh" id="pass" type="button"><span id="show_pass"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></span></button>
        </span>
    </div>
    <button type="submit" class="btn btn-primary btn-block md" id="btn-login"><span id="status">Login</span></button>
    <?=form_close()?>
</div>
</div>
