<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="id">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" href="<?=site_url('assets/img/logo.png')?>">
        <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1">
        <title><?=$title?></title>
        <?=link_tag('assets/css/bootstrap.css')?>
        <?=link_tag('assets/css/style.css')?>

    </head>
    <body onLoad="waktu()">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="http://simanisbontang.com/home/menuutama"><i class="glyphicon glyphicon-home"></i> Home</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <a class="navbar-brand">
                         <div id="jam-digital">
                         <i class="glyphicon glyphicon-time"></i>
                            <span id="jam"></span> :
                            <span id="menit"></span> :
                            <span id="detik"></span>
                        </div>
                    </a>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <?php $this->load->view($content)?>
        </div>
        <script>
    // var timerInstance = new easytimer.Timer();
    window.setTimeout("waktu()",1000);
    function addZero(i) {
      if (i < 10) {
        i = "0" + i;
    }
    return i;
}

    function waktu() {
        var tanggal = new Date();
        setTimeout("waktu()",1000);
        document.getElementById("jam").innerHTML = addZero(tanggal.getHours());
        document.getElementById("menit").innerHTML = addZero(tanggal.getMinutes());
        document.getElementById("detik").innerHTML = addZero(tanggal.getSeconds());
    }
</script>
        <?php $this->load->view('include/footer.php') ?>
    </body>
</html>
