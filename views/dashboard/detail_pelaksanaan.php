<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2><?=$form_title?> <a class="btn btn-primary btn-sm pull-right" href="<?=site_url('mytask')?>"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Kembali</a></h2>
<hr>
<? $jenis = ($pelaksanaan['jenis_st']==1)?'Pembinaan':'Pengawasan'; ?>
<table class="table table-bordered table-striped">
    <tr>
        <th colspan="2" class="text-center"><?= $jenis ?></th>
    </tr>
    <tr>
        <th width="15%"> Nomor ST </th>
        <td> <?= $pelaksanaan['nomor_st'] ?> </td>
    </tr>
    <tr>
        <th> Tanggal ST </th>
        <td> <?= $pelaksanaan['tanggal_st'] ?> </td>
    </tr>
    <tr>
        <th> Dari </th>
        <td> <?= $pelaksanaan['u_fname'] ?> </td>
    </tr>
    <tr>
        <th> Keterangan </th>
        <td> <?= $pelaksanaan['keterangan'] ?> </td>
    </tr>
    <tr>
        <th> File ST </th>
        <td> <a href="<?=site_url('uploads/ST/'.$jenis.'/'.$pelaksanaan['file_st'])?>" target="_blank" ><?=$pelaksanaan['file_st']?></a> </td>
    </tr>
</table>
<hr>
<h3>File Pendukung</h3>
<table class="table table-bordered table-striped">
    <tr>
        <th>No</th>
        <th>File Pendukung</th>
        <th>Keterangan</th>
    </tr>
    <? $no=1; foreach ($det_pelaksanaan as $key => $value) { ?>
        <tr>
            <td> <?= $no ?> </td>
            <td> <a href="<?= base_url()?>uploads/pendukung/<?= $value['file_pendukung'] ?>" target="_blank"> 
            <?= $value['file_pendukung'] ?></a>
            </td>
            <td> <?= $value['keterangan'] ?> </td>
        </tr>
    <? $no++;} ?>
</table>
<hr>
 <div class="form-group">
        <div class="col-md-12 text-center">
            <a class="btn btn-danger" href="<?=site_url('laporan/add/'.$pelaksanaan["id"].'')?>"><i class="glyphicon glyphicon-plus"></i> Buat Laporan</a>
        </div>
    </div>
