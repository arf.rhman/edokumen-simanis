<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>



<h2><?=$form_title?></h2>

<hr>

<?=form_open_multipart($action, 'class="form-horizontal"')?>

        <div class="form-group">

        <label class="col-sm-2 control-label">Nomor ST</label>

        <div class="col-sm-4">

            <input type="text" name="nomor_st" class="form-control" value="<?=isset($pelaksanaan['nomor_st']) ? $pelaksanaan['nomor_st'] : set_value('nomor_st')?>" placeholder="Nomor ST" required>

            <!-- <small class="text-danger"><?=form_error('nomor_st')?></small> -->

        </div>

    </div>

      <div class="form-group">

        <label class="col-sm-2 control-label">Nomor Laporan</label>

        <div class="col-sm-4">

            <input type="text" name="nomor_laporan" class="form-control" value="<?=isset($laporan['nomor_laporan']) ? $laporan['nomor_laporan'] : set_value('nomor_laporan')?>" placeholder="Nomor Laporan">

            <!-- <small class="text-danger"><?=form_error('nomor_laporan')?></small> -->

        </div>

    </div>

    <div class="form-group">

        <label class="col-sm-2 control-label">Judul Laporan</label>

        <div class="col-sm-4">

            <textarea class="form-control" required placeholder="Judul Laporan" name="judul_laporan"></textarea>

            <!-- <small class="text-danger"><?=form_error('s_nisn')?></small> -->

        </div>

    </div>

    <div class="form-group">

        <label class="col-sm-2 control-label">Tanggal Laporan</label>

        <div class="col-sm-3">

            <input type="date" name="tanggal_laporan" class="form-control" value="<?=isset($laporan['tanggal_laporan']) ? $laporan['tanggal_laporan'] : set_value('tanggal_laporan')?>" placeholder="Tanggal Lahir">

            <!-- <small class="text-danger"><?=form_error('s_dob')?></small> -->

        </div>

    </div>

     <div class="form-group">

        <label class="col-sm-2 control-label">Keterangan Laporan</label>

        <div class="col-sm-8">

            <textarea class="form-control" name="keterangan_laporan"><?=isset($laporan['keterangan_laporan']) ? $laporan['keterangan_laporan'] : set_value('keterangan_laporan')?></textarea>

            <!-- <small class="text-danger"><?=form_error('keterangan_laporan')?></small> -->

        </div>

    </div>

   <!-- 

    <div class="form-group">

        <label class="col-sm-2 control-label">Program Keahlian</label>

        <div class="col-sm-3">

            <select name="s_mid" class="form-control" required>

                <option value="<?=isset($student['s_mid']) ? $student['s_mid'] : set_value('s_mid')?>"><?=isset($student['s_mid']) ? $student['s_mid'] : set_value('s_mid')?></option>

                <?php foreach (array_reverse($majors) as $row) {     ?>

                    <option value="<?=$row['m_id']?>"><?=$row['m_name']?></option>

                <?php } ?>

            </select>

            <small class="text-danger"><?=form_error('s_mid')?></small>

        </div>

    </div> -->



    <div class="form-group">

        <label class="col-sm-2 control-label">File Laporan</label>

        <div class="col-sm-10">

            <?php if ($this->uri->segment(2) == 'edit') { ?>

                <?php if (file_exists('./uploads/laporan/' . $laporan['file_laporan'])) { ?>

                    <a href="<?=site_url('uploads/laporan/' . $laporan['file_laporan'])?>" target="_blank" ><?=$laporan['file_laporan']?></a>

                <?php } ?>

                <input type="file" name="file_laporan">

               <small class="help-block">Format file yang diperbolehkan *.pdf, *.doc, *.jpg, *.png  dan ukuran maksimal 10 MB.</small>

                <!-- <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small> -->

            <?php } else { ?>

                <input type="file" name="file_laporan">

                <small class="help-block">Format file yang diperbolehkan *.pdf, *.doc, *.jpg, *.png  dan ukuran maksimal 10 MB.</small>

                <!-- <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small> -->

            <?php } ?>

        </div>



        <label class="col-sm-2 control-label">Link Google Drive</label>

        <div class="col-sm-4">

            <input type="text" name="link_gdrive" class="form-control" value="<?=isset($laporan['link_gdrive']) ? $laporan['link_gdrive'] : set_value('link_gdrive')?>" placeholder="Link Google Drive">

            <!-- <small class="text-danger"><?=form_error('link_gdrive')?></small> -->

        </div>

    </div>

    <hr>

    <h3>Share</h3>

     <div class="form-group">

        <label class="col-sm-2 control-label">Pilih Inspektur Pembantu</label>

        <div class="col-sm-4">

           <select required name="tujuan_laporan" class="form-control">

                <?php foreach ($irban as $key => $value) { ?>

                     <option value="<?= $value['u_id'] ?>" <?= (isset($laporan['tujuan_laporan']) && ($laporan['tujuan_laporan'] == $value['u_id'])) ? 'selected':''?>><?= $value['u_fname'] ?></option>

                <? } ?>

            </select>

            <!-- <small class="text-danger"><?=form_error('s_grade')?></small> -->

        </div>

    </div>

    <!-- <hr> -->

    <!-- <h3>Lampiran</h3> -->

<!--     <p class="help-block">Format gambar yang diperbolehkan *.png, *.jpg dan ukuran maksimal 2 MB.</p>

    <hr>

    <div class="form-group">

        <label class="col-sm-2 control-label">Kartu Keluarga</label>

        <div class="col-sm-10">

            <?php if ($this->uri->segment(2) == 'edit') { ?>

                <?php if (file_exists('./uploads/kk/' . $student['s_kk'])) { ?>

                    <a href="<?=site_url('uploads/kk/' . $student['s_kk'])?>" target="_blank" ><?=$student['s_kk']?></a>

                <?php } ?>

                <input type="file" name="s_kk">

                <small class="text-danger"><?=!empty($err_kk) ? $err_kk : "";?></small>

            <?php } else { ?>

                <input type="file" name="s_kk">

                <small class="text-danger"><?=!empty($err_kk) ? $err_kk : "";?></small>

            <?php } ?>

        </div>

    </div>

    <div class="form-group">

        <label class="col-sm-2 control-label">KTP Ayah</label>

        <div class="col-sm-10">

            <?php if ($this->uri->segment(2) == 'edit') { ?>

                <?php if (file_exists('./uploads/ktpa/' . $student['s_ktpa'])) { ?>

                    <a href="<?=site_url('uploads/ktpa/' . $student['s_ktpa'])?>" target="_blank" ><?=$student['s_ktpa']?></a>

                <?php } ?>

                <input type="file" name="s_ktpa">

                <small class="text-danger"><?=!empty($err_ktpa) ? $err_ktpa : "";?></small>

            <?php } else { ?>

                <input type="file" name="s_ktpa">

                <small class="text-danger"><?=!empty($err_ktpa) ? $err_ktpa : "";?></small>

            <?php } ?>

        </div>

    </div>

    <div class="form-group">

        <label class="col-sm-2 control-label">KTP Ibu</label>

        <div class="col-sm-10">

            <?php if ($this->uri->segment(2) == 'edit') { ?>

                <?php if (file_exists('./uploads/ktpi/' . $student['s_ktpi'])) { ?>

                    <a href="<?=site_url('uploads/ktpi/' . $student['s_ktpi'])?>" target="_blank" ><?=$student['s_ktpi']?></a>

                <?php } ?>

                <input type="file" name="s_ktpi">

                <small class="text-danger"><?=!empty($err_ktpi) ? $err_ktpi : "";?></small>

            <?php } else { ?>

                <input type="file" name="s_ktpi">

                <small class="text-danger"><?=!empty($err_ktpi) ? $err_ktpi : "";?></small>

            <?php } ?>

        </div>

    </div>

    <div class="form-group">

        <label class="col-sm-2 control-label">KIP / KPS</label>

        <div class="col-sm-10">

            <?php if ($this->uri->segment(2) == 'edit') { ?>

                <?php if (file_exists('./uploads/kips/' . $student['s_kips'])) { ?>

                    <a href="<?=site_url('uploads/kips/' . $student['s_kips'])?>" target="_blank" ><?=$student['s_kips']?></a>

                <?php } ?>

                <input type="file" name="s_kips">

                <small class="text-danger"><?=!empty($err_kips) ? $err_kips : "";?></small>

            <?php } else { ?>

                <input type="file" name="s_kips">

                <small class="text-danger"><?=!empty($err_kips) ? $err_kips : "";?></small>

            <?php } ?>

        </div>

    </div>

    <div class="form-group">

        <label class="col-sm-2 control-label">SK Tidak Mampu</label>

        <div class="col-sm-10">

            <?php if ($this->uri->segment(2) == 'edit') { ?>

                <?php if (file_exists('./uploads/sktm/' . $student['s_sktm'])) { ?>

                    <a href="<?=site_url('uploads/sktm/' . $student['s_sktm'])?>" target="_blank" ><?=$student['s_sktm']?></a>

                <?php } ?>

                <input type="file" name="s_sktm">

                <small class="text-danger"><?=!empty($err_sktm) ? $err_sktm : "";?></small>

            <?php } else { ?>

                <input type="file" name="s_sktm">

                <small class="text-danger"><?=!empty($err_sktm) ? $err_sktm : "";?></small>

            <?php } ?>

        </div>

    </div>

    <div class="form-group">

        <label class="col-sm-2 control-label">Ijazah</label>

        <div class="col-sm-10">

            <?php if ($this->uri->segment(2) == 'edit') { ?>

                <?php if (file_exists('./uploads/ijazah/' . $student['s_ijazah'])) { ?>

                    <a href="<?=site_url('uploads/ijazah/' . $student['s_ijazah'])?>" target="_blank" ><?=$student['s_ijazah']?></a>

                <?php } ?>

                <input type="file" name="s_ijazah">

                <small class="text-danger"><?=!empty($err_ijazah) ? $err_ijazah : "";?></small>

            <?php } else { ?>

                <input type="file" name="s_ijazah">

                <small class="text-danger"><?=!empty($err_ijazah) ? $err_ijazah : "";?></small>

            <?php } ?>

        </div>

    </div>

    <div class="form-group">

        <label class="col-sm-2 control-label">SKHUN</label>

        <div class="col-sm-10">

            <?php if ($this->uri->segment(2) == 'edit') { ?>

                <?php if (file_exists('./uploads/skhun/' . $student['s_skhun'])) { ?>

                    <a href="<?=site_url('uploads/skhun/' . $student['s_skhun'])?>" target="_blank" ><?=$student['s_skhun']?></a>

                <?php } ?>

                <input type="file" name="s_skhun">

                <small class="text-danger"><?=!empty($err_skhun) ? $err_skhun : "";?></small>

            <?php } else { ?>

                <input type="file" name="s_skhun">

                <small class="text-danger"><?=!empty($err_skhun) ? $err_skhun : "";?></small>

            <?php } ?>

        </div>

    </div> -->

    <hr>



<!--     <h3>Status Kelengkapan Data</h3>

    <p class="help-block">Pilih status kelengkapan data berdasarkan lampiran.</p>

    <hr>

    <div class="form-group">

        <label class="col-sm-2 control-label">Status Data</label>

        <div class="col-sm-3">

            <select id="s_status" name="s_status" class="form-control" required>

                <option></option>

                <option value="Belum Ada Data">Belum Ada Data</option>

                <option value="Kurang">Kurang</option>

                <option value="Lengkap">Lengkap</option>

            </select>

        </div>

    </div> -->



    <div class="form-group">

        <div class="col-sm-offset-2 col-sm-10">

            <!-- <small class="help-block hint">Tombol simpan akan muncul setelah Anda memilih status kelengkapan data.</small> -->

            <!-- <br> -->
            <input type="hidden" name="tujuan_laporan_opd" value="<?=isset($pelaksanaan['tujuan_opd_st']) ? $pelaksanaan['tujuan_opd_st'] : set_value('tujuan_opd_st')?>">
            
            <button type="submit" name="submit" class="btn btn-success" id="submit">Simpan</button>&nbsp;

            <a class="btn btn-default" href="<?=site_url('pelaksanaan')?>">Kembali</a>

        </div>

    </div>

<?=form_close()?>

