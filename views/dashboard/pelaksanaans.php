<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2>Data Pelaksanaan <a class="btn btn-danger btn-sm pull-right" href="<?=site_url('pelaksanaan/add')?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Data</a>
<!-- <a class="btn btn-success btn-sm add" href="<?=site_url('student/import')?>">Import Data</a> -->
</h2>
<div class="table">
    <table id="pelaksanaan" class="display table table-bordered table-hover table-responsive">
        <thead>
            <tr>
                <!-- <th width="5%">No</th> -->
                <th width="12%">Nomor ST</th>
                <th width="10%">Tanggal ST</th>
               <!--  <th width="20%">Uraian</th>
                <th width="8%">Jenis ST</th>
                <th width="10%">File / <br>Tanggal Upload</th>
                <th width="15%">Tujuan KT / <br>Tujuan OPD</th>
                <th width="15%">Tindakan</th> -->
            </tr>
        </thead>
        <tbody>
       <!--  <tr>
            <td>1 </td>
            <td>ST-201/U02/2019</td>
            <td> 6 Oktober 2019 </td>
            <td>Pemeriksaan Reguler</td>
            <td><label class="label label-primary">Pembinaan</label></td>
            <td> <a href="#"> <b>Berkas.pdf</b> </a> / <br> 10 Oktober 2019 </td>
            <td> Haryadi / <br> Camat Bontang Barat</td>
            <td> 
            <a class="btn btn-primary btn-sm mb"><span class="glyphicon glyphicon-share" aria-hidden="true"></span></a>
            <a class="btn btn-success btn-sm mb" href="#" target="_blank" title="Download">
            <span class="glyphicon glyphicon-download" aria-hidden="true"></span></a>
             <a class="btn btn-warning btn-sm mb" href="#">
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
             <a class="btn btn-danger btn-sm mb" href="#">
            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
            </td>
        </tr>
        <tr>
            <td>2 </td>
            <td>ST-208/U02/2019</td>
            <td> 1 Oktober 2019 </td>
            <td>Pemeriksaan Reguler</td>
            <td><label class="label label-danger">Pengawasan</label></td>
            <td> <a href="#"> <b>Berkas.pdf</b> </a> / <br> 1 Oktober 2019 </td>
            <td> Haryadi / <br> Camat Bontang Barat</td>
            <td> 
            <a class="btn btn-primary btn-sm mb"><span class="glyphicon glyphicon-share" aria-hidden="true"></span></a>
            <a class="btn btn-success btn-sm mb" href="#" target="_blank" title="Download">
            <span class="glyphicon glyphicon-download" aria-hidden="true"></span></a>
             <a class="btn btn-warning btn-sm mb" href="#">
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
             <a class="btn btn-danger btn-sm mb" href="#">
            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
            </td>
        </tr>
        <tr>
            <td>3</td>
            <td>ST-221/U02/2019</td>
            <td> 20 Oktober 2019 </td>
            <td>Pemeriksaan Reguler</td>
            <td><label class="label label-primary">Pembinaan</label></td>
            <td><a href="#"> <b>Berkas.pdf</b> </a> / <br>  20 Oktober 2019 </td>
            <td> Haryadi / <br> Camat Bontang Utara</td>
            <td> 
            <a class="btn btn-primary btn-sm mb"><span class="glyphicon glyphicon-share" aria-hidden="true"></span></a>
            <a class="btn btn-success btn-sm mb" href="#" target="_blank" title="Download">
            <span class="glyphicon glyphicon-download" aria-hidden="true"></span></a>
             <a class="btn btn-warning btn-sm mb" href="#">
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
             <a class="btn btn-danger btn-sm mb" href="#">
            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
            </td>
        </tr> -->
        </tbody>
    </table>
    <script>
        function confirmDialog() {
            return confirm("Apakah Anda yakin akan menghapus data ini?")
        }
    </script>
</div>
