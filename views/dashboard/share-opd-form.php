<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2><?=$form_title?> <a class="btn btn-primary btn-sm pull-right" href="<?=site_url('mytask')?>"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Kembali</a></h2>
<hr>
<? $jenis = ($pelaksanaan['jenis_st']==1)?'Pembinaan':'Pengawasan'; ?>
<table class="table table-bordered table-striped">
    <tr>
        <th colspan="2" class="text-center"><?= $jenis ?></th>
    </tr>
    <tr>
        <th width="15%"> Nomor ST </th>
        <td> <?= $pelaksanaan['nomor_st'] ?> </td>
    </tr>
    <tr>
        <th> Tanggal ST </th>
        <td> <?= $pelaksanaan['tanggal_st'] ?> </td>
    </tr>
    <tr>
        <th> Uraian ST </th>
        <td> <?= $pelaksanaan['uraian_st'] ?> </td>
    </tr>
    <tr>
        <th> File ST </th>
        <td> <a href="<?=site_url('uploads/ST/'.$jenis.'/'.$pelaksanaan['file_st'])?>" target="_blank" ><?=$pelaksanaan['file_st']?></a> </td>
    </tr>
</table>
<?=form_open_multipart($action, 'class="form-horizontal"')?>
<hr>
    <h3>Share</h3>
    <div class="form-group">
        <label class="col-sm-2 control-label">Keterangan Tambahan</label>
        <div class="col-sm-8">
            <textarea class="form-control" required name="keterangan"><?=isset($pelaksanaan['keterangan']) ? $pelaksanaan['keterangan'] : set_value('keterangan')?></textarea>
            <small class="text-danger"><?=form_error('keterangan')?></small>
        </div>
    </div>


     <div class="form-group">
        <label class="col-sm-2 control-label">Pilih OPD</label>
        <div class="col-sm-4">
            <select required name="tujuan_opd_st" class="form-control">
                <?php foreach ($opd as $key => $value) { ?>
                     <option value="<?= $value['u_id'] ?>" <?= (isset($pelaksanaan['tujuan_opd_st']) && ($pelaksanaan['tujuan_opd_st'] == $value['u_id'])) ? 'selected':''?>><?= $value['u_fname'] ?></option>
                <? } ?>
            </select>
            <small class="text-danger"><?=form_error('tujuan_kt_st')?></small>
        </div>
    </div>
  
    <hr>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <!-- <small class="help-block hint">Tombol simpan akan muncul setelah Anda memilih status kelengkapan data.</small> -->
            <!-- <br> -->
            <button type="submit" name="submit" class="btn btn-success" id="submit">Simpan</button>&nbsp;
            <a class="btn btn-default" href="<?=site_url('mytask')?>">Kembali</a>
        </div>
    </div>
<?=form_close()?>
