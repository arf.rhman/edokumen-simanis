<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>



<h2>My Task </h2>

<div class="table-responsive">

    <table id="pelaksanaans" class="display table table-bordered table-hover table-responsive">

        <thead>

            <tr>

                <th width="3%">No</th>

                <th width="10%">Nomor ST</th>

                <th width="10%">Tanggal ST</th>

                <th width="15%">Uraian</th>

                <th width="10%">Jenis ST</th>

                <th width="15%">File / <br>Tanggal Upload</th>

                <th width="12%">Pembuat ST</th>

                <th width="12%">Tujuan KT / <br> Tujuan OPD</th>

                <!-- <th width="8%">Posisi</th> -->

                <th width="20%">Tindakan</th>

            </tr>

        </thead>

        <tbody>

            <? $no=1; foreach ($pelaksanaan as $key => $value) {
                
                $label = '';

                $jenis = ''; 

                if($value['jenis_st']==2){

                    $label = '<label class="label label-danger">Pengawasan</label>';

                    $jenis = 'Pengawasan';

                }else{

                   $label = '<label class="label label-primary">Pembinaan</label>';

                    $jenis = 'Pembinaan';

                }

            ?>

            <tr>

                <td><?= $no ?></td>

                <td><?= $value['nomor_st'] ?></td>

                <td><?= $value['tanggal_st'] ?></td>

                <td><?= $value['uraian_st'] ?></td>

                <td><?= $label ?></td>

                <td><a href="<?= base_url()?>uploads/ST/<?= $jenis ?>/<?= $value['file_st'] ?>" target="_blank"> 

                <?= substr($value['file_st'], 0,15)?>... </a> / <br><?= $value['tanggal_upload_st'] ?></td>

                <td style="color: red"><?= $value['nama_pembuat_st'] ?></td>

                <td><?= $value['nama_kt'] ?> / <br> <?= ($value['nama_opd'])?$value['nama_opd']:'-' ?></td>

                <!-- <td> -->

                    <!-- <label class="label label-<?= $label_posisi[$value['status_posisi_st']]?>"><?= $posisi[$value['status_posisi_st']] ?></label> -->

                <!-- </td> -->

                <td>

                    <? if($value['status_posisi_st']==3){?>

                     <a class="btn btn-primary btn-sm mb" href="<?= site_url('mytask/detail/'.$value['id'])?>"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Detail </a>

                   <a class="btn btn-danger btn-sm mb" href="<?= site_url('laporan/add/'.$value['id'])?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Laporan </a>

                    <?}else{?>

                      <a class="btn btn-success btn-sm mb" href="<?= base_url()?>uploads/ST/<?= $jenis ?>/<?= $value['file_st'] ?>" target="_blank" title="Download">

                    <span class="glyphicon glyphicon-download" aria-hidden="true"></span></a>

                   <a class="btn btn-danger btn-sm mb" href="<?= site_url('mytask/share_opd/'.$value['id'])?>"><span class="glyphicon glyphicon-share" aria-hidden="true"></span> Set OPD</a>

                        <?}?> 

                 

                </td>

            <? $no++; }?>

            </tr>

        </tbody>

    </table>

    <script>

        function confirmDialog() {

            return confirm("Apakah Anda yakin akan menghapus data ini?")

        }

    </script>

</div>

