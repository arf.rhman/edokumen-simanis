<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    .icon{
    -webkit-transition: all .3s linear;
    -o-transition: all .3s linear;
    transition: all .3s linear;
    position: absolute;
    top: 5%;
    right: 7%;
    z-index: 0;
    font-size: 90px;
    color: rgba(0, 0, 0, 0.25);
    }
    .bg-yellow{
        background-color: #f39c12;
        color: white;
    }
     .bg-red{
        background-color: #dd4b39;
        color: white;
    }
    .bg-green{
        background-color: #00a65a;
        color: white;  
    }
</style>
<div>
<h1>
    Selamat Datang <small><?=$this->session->userdata['u_fname']?></small> <small>(<?=$this->session->userdata['u_level']?>)</small>
    <? if($this->session->userdata['u_level']!='OPD'){?>
    <form method="POST"> 
    <span style="font-size: 11pt;">
    Pilih OPD : 
        <select name="filteropd" id="filteropd" style="font-size: 11pt;padding: 5px;min-width: 25%">
            <option value="all">Semua</option>
            <?php foreach ($opd as $key => $value) { ?>
                <option value="<?= $value->tujuan_opd_st ?>" <?if(!empty($_POST['filteropd'])){
                    echo ($_POST['filteropd']==$value->tujuan_opd_st)?'selected':'';
                    }?>><?= $value->u_instansi ?></option>
            <?php } ?>
        </select>
<button type="submit" class="btn btn-sm btn-info"> Filter OPD</button>
<!-- <a href="<?=base_url()?>dashboard/export" class="btn btn-sm btn-success"> Export Excel</a> -->
<button type="button" class="btn btn-sm btn-success" onClick="exportExcel()"> Export Excel</button>
</span>
</form>
<? } ?>
</h1>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Dashboard Dokumen</h3>
            </div>
            <div class="panel-body row">
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-body bg-yellow">
                            <h1 class="ttl"><?= count($pembinaan) ?></h1>
                            <h3>Pembinaan</h3>
                            <div class="icon"><i class="glyphicon glyphicon-time"></i></div>
                        </div>
                        <div class="panel-footer bg-yellow">Total Dokumen Pembinaan</div>

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-body bg-red">
                            <h1 class="ttl"><?= count($pengawasan) ?></h1>
                            <h3>Pengawasan</h3>
                            <div class="icon"><i class="glyphicon glyphicon-time"></i></div>
                        </div>
                        <div class="panel-footer bg-red">Total Dokumen Pembinaan</div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-body bg-green">
                            <h1 class="ttl"><?= count($total) ?></h1>
                            <h3>Dokumen</h3>
                            <div class="icon"><i class="glyphicon glyphicon-time"></i></div>
                        </div>
                        <div class="panel-footer bg-green">Total Semua Dokumen</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function exportExcel() {
        var filteropd = $("#filteropd").val();
        window.location.href = '<?=base_url()?>dashboard/export?q='+filteropd;
    }
</script>
