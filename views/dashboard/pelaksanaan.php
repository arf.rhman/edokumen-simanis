<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2>Data Pelaksanaan <a class="btn btn-danger btn-sm pull-right" href="<?=site_url('pelaksanaan/add')?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Data</a>

</h2>
<div class="table-responsive">
    <table id="pelaksanaans" class="display table table-bordered table-hover table-responsive">
        <thead>
            <tr>
                 <th width="5%">No</th>
                <th width="12%">Nomor ST</th>
                <th width="10%">Tanggal ST</th>
                <th width="20%">Uraian</th>
                <th width="8%">Jenis ST</th>
                <th width="15%">File / <br>Tanggal Upload</th>
                <th width="15%">Tujuan KT / <br> Tujuan OPD</th>
                <th width="8%">Posisi</th>
                <th width="12%">Tindakan</th>
            </tr>
        </thead>
        <tbody>
            <? $no=1; foreach ($pelaksanaan as $key => $value) {
                $label = '';
                $jenis = ''; 
                if($value['jenis_st']==2){
                    $label = '<label class="label label-danger">Pengawasan</label>';
                    $jenis = 'Pengawasan';
                }else{
                   $label = '<label class="label label-primary">Pembinaan</label>';
                    $jenis = 'Pembinaan';
                }
            ?>
            <tr>
                <td><?= $no ?></td>
                <td><?= $value['nomor_st'] ?></td>
                <td><?= $value['tanggal_st'] ?></td>
                <td><?= $value['uraian_st'] ?></td>
                <td><?= $label ?></td>
                <td><a href="<?= base_url()?>uploads/ST/<?= $jenis ?>/<?= $value['file_st'] ?>" target="_blank"> 
                <?= substr($value['file_st'], 0,15)?>... </a> / <br><?= $value['tanggal_upload_st'] ?></td>
                <td><?= $value['nama_kt'] ?> / <br> <?= ($value['nama_opd'])?$value['nama_opd']:'-' ?></td>
                <td>
                    <? if($value['status_st']==1){ ?>

                    <label class="label label-success">Laporan sudah di OPD</label>

                    <?    }else{ ?>
                    <label class="label label-<?= $label_posisi[$value['status_posisi_st']]?>"><?= $posisi[$value['status_posisi_st']] ?></label>
                    <?}?>
                </td>
                <td>
                   <!-- <a class="btn btn-primary btn-sm mb"><span class="glyphicon glyphicon-share" aria-hidden="true"></span></a> -->
                   <a class="btn btn-success btn-sm mb" href="<?= base_url()?>uploads/ST/<?= $jenis ?>/<?= $value['file_st'] ?>" target="_blank" title="Download">
                    <span class="glyphicon glyphicon-download" aria-hidden="true"></span></a>
                    <? if($value['status_posisi_st']<2 && $value['status_st']==0){ ?>


                    <a class="btn btn-warning btn-sm mb" href="<?= site_url('pelaksanaan/edit/'.$value['id'])?>">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                        <a class="btn btn-danger btn-sm mb" onclick="return confirmDialog();" href="
                        <?= site_url('pelaksanaan/delete/'.$value['id'])?>"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                        <? } ?>
                </td>
            <? $no++;}?>
            </tr>
        </tbody>
    </table>
    <script>
        function confirmDialog() {
            return confirm("Apakah Anda yakin akan menghapus data ini?")
        }

        function confirmDialogStatus() {
            <?php if ($this->session->userdata['u_level'] == "Administrator") { ?>
                return confirm("Apakah Anda yakin akan mengubah status data ini menjadi tidak aktif?")
            <?php } else { ?>
                window.alert("Hanya Administrator yang boleh mengubah status data!")
            <?php } ?>
        }
    </script>
</div>
