<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2>Data User OPD <a class="btn btn-success btn-sm add" href="<?=site_url('useropd/add')?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Tambah Data</a></h2>
<div class="table">
    <table id="useropd" class="display table table-bordered table-hover table-responsive">
        <thead>
            <tr>
                <th width="5%">No</th>
                <th width="12%">Username</th>
                <th width="10%">NIP</th>
                <th width="22%">Nama Lengkap</th>
                <th width="12%">Instansi</th>
                <th width="8%">Status</th>
                <th width="17%">Tindakan</th>
            </tr>
        </thead>
    </table>
    <script>
        function confirmDialog() {
            return confirm("Apakah Anda yakin akan mereset password user ini?")
        }
    </script>
</div>
