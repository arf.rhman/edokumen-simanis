<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2>My Task </h2>
<div class="table-responsive">
    <table id="pelaksanaans" class="display table table-bordered table-hover table-responsive">
        <thead>
            <tr>
                <th width="3%">No</th>
                <th width="15%">Nomor ST  / <br> Tanggal ST</th>
                <th width="15%">File / <br>Tanggal Upload</th>
                <th width="15%">Keterangan</th>
                <th width="12%">Dari</th>
                <th width="15%">Tindakan</th>
            </tr>
        </thead>
        <tbody>
            <? $no=1; foreach ($pelaksanaan as $key => $value) {
                $label = '';
                $jenis = ''; 
                if($value['jenis_st']==2){
                    $label = '<label class="label label-danger">Pengawasan</label>';
                    $jenis = 'Pengawasan';
                }else{
                   $label = '<label class="label label-primary">Pembinaan</label>';
                    $jenis = 'Pembinaan';
                }
            ?>
            <tr>
                <td><?= $no ?></td>
                <td><?= $value['nomor_st'] ?> / <br> <?= $value['tanggal_st'] ?></td>
                <td><a href="<?= base_url()?>uploads/ST/<?= $jenis ?>/<?= $value['file_st'] ?>" target="_blank"> 
                <?= substr($value['file_st'], 0,15)?>... </a> / <br><?= $value['tanggal_upload_st'] ?></td>
                <td><?= $value['keterangan'] ?></td>
                <td><?= $value['nama_kt'] ?> </td>
                <td>
                   <a class="btn btn-danger btn-sm mb" href="<?= site_url('mytask/proses_st/'.$value['id'])?>"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span> Proses</a>
                   <a class="btn btn-success btn-sm mb" href="<?= base_url()?>uploads/ST/<?= $jenis ?>/<?= $value['file_st'] ?>" target="_blank" title="Download">
                    <span class="glyphicon glyphicon-download" aria-hidden="true"></span> Download ST</a>
                </td>
            <? $no++; }?>
            </tr>
        </tbody>
    </table>
    <script>
        function confirmDialog() {
            return confirm("Apakah Anda yakin akan menghapus data ini?")
        }
    </script>
</div>
