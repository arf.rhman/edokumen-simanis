<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<h2><?=$form_title?> <a class="btn btn-primary btn-sm pull-right" href="<?=site_url('pelaksanaan')?>"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Kembali</a></h2>
<hr>
<?=form_open_multipart($action, 'class="form-horizontal"')?>
        <div class="form-group">
        <label class="col-sm-2 control-label">Nomor ST</label>
        <div class="col-sm-4">
            <input type="text" name="nomor_st" class="form-control" value="<?=isset($pelaksanaan['nomor_st']) ? $pelaksanaan['nomor_st'] : set_value('nomor_st')?>" placeholder="Nomor ST" required>
            <small class="text-danger"><?=form_error('nomor_st')?></small>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Uraian ST</label>
        <div class="col-sm-4">
            <textarea class="form-control" required name="uraian_st"><?=isset($pelaksanaan['uraian_st']) ? $pelaksanaan['uraian_st'] : set_value('uraian_st')?></textarea>
            <small class="text-danger"><?=form_error('uraian_st')?></small>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Tanggal ST</label>
        <div class="col-sm-3">
            <input type="date" name="tanggal_st" class="form-control" value="<?=isset($pelaksanaan['tanggal_st']) ? $pelaksanaan['tanggal_st'] : set_value('tanggal_st')?>" placeholder="Tanggal ST" required>
            <small class="text-danger"><?=form_error('tanggal_st')?></small>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Jenis Dokumen</label>
        <div class="col-sm-3">
            <select name="jenis_st" class="form-control" required>
               <!--  <option value="<?=isset($pelaksanaan['jenis_st']) ? $pelaksanaan['jenis_st'] : set_value('jenis_st')?>"><?=isset($pelaksanaan['jenis_st']) ? $pelaksanaan['jenis_st'] : set_value('jenis_st')?></option> -->
                <option value="1" <?= (isset($pelaksanaan['jenis_st']) && ($pelaksanaan['jenis_st'] == 1)) ? 'selected':''?>>Pembinaan</option>
                <option value="2" <?= (isset($pelaksanaan['jenis_st']) && ($pelaksanaan['jenis_st'] == 2)) ? 'selected':''?>>Pengawasan</option>
            </select>
            <small class="text-danger"><?=form_error('jenis_st')?></small>
        </div>
    </div>

   <div class="form-group">
        <label class="col-sm-2 control-label">File ST</label>
        <div class="col-sm-10">
            <?php if ($this->uri->segment(2) == 'edit') { 
                $jenis = ($pelaksanaan['jenis_st']==1)?'Pembinaan':'Pengawasan';
                ?>
                <?php if (file_exists('./uploads/ST/'.$jenis.'/'.$pelaksanaan['file_st'])) { ?>
                    <a href="<?=site_url('uploads/ST/'.$jenis.'/'.$pelaksanaan['file_st'])?>" target="_blank" ><?=$pelaksanaan['file_st']?></a>
                <?php } ?>
                <input type="file" name="file_st">
                <small class="help-block">Format file yang diperbolehkan *.pdf, *.doc dan ukuran maksimal 10 MB.</small>
                <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small>
            <?php } else { ?>
                <input type="file" name="file_st">
                <small class="help-block">Format file yang diperbolehkan *.pdf, *.doc, *.jpg, *.png  dan ukuran maksimal 10 MB.</small>
                <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small>
            <?php } ?>
        </div>
    </div>
    <hr>
    <h3>Share</h3>
     <div class="form-group">
        <label class="col-sm-2 control-label">Pilih Ketua Tim</label>
        <div class="col-sm-4">
            <select required name="tujuan_kt_st" class="form-control">
                <?php foreach ($ketua_tim as $key => $value) { ?>
                     <option value="<?= $value['u_id'] ?>" <?= (isset($pelaksanaan['tujuan_kt_st']) && ($pelaksanaan['tujuan_kt_st'] == $value['u_id'])) ? 'selected':''?>><?= $value['u_fname'] ?></option>
                <? } ?>
            </select>
            <small class="text-danger"><?=form_error('tujuan_kt_st')?></small>
        </div>
    </div>
    <!-- <hr> -->
    <!-- <h3>Lampiran</h3> -->
<!--     <p class="help-block">Format gambar yang diperbolehkan *.png, *.jpg dan ukuran maksimal 2 MB.</p>
    <hr>
    <div class="form-group">
        <label class="col-sm-2 control-label">Kartu Keluarga</label>
        <div class="col-sm-10">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/kk/' . $pelaksanaan['s_kk'])) { ?>
                    <a href="<?=site_url('uploads/kk/' . $pelaksanaan['s_kk'])?>" target="_blank" ><?=$pelaksanaan['s_kk']?></a>
                <?php } ?>
                <input type="file" name="s_kk">
                <small class="text-danger"><?=!empty($err_kk) ? $err_kk : "";?></small>
            <?php } else { ?>
                <input type="file" name="s_kk">
                <small class="text-danger"><?=!empty($err_kk) ? $err_kk : "";?></small>
            <?php } ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">KTP Ayah</label>
        <div class="col-sm-10">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/ktpa/' . $pelaksanaan['s_ktpa'])) { ?>
                    <a href="<?=site_url('uploads/ktpa/' . $pelaksanaan['s_ktpa'])?>" target="_blank" ><?=$pelaksanaan['s_ktpa']?></a>
                <?php } ?>
                <input type="file" name="s_ktpa">
                <small class="text-danger"><?=!empty($err_ktpa) ? $err_ktpa : "";?></small>
            <?php } else { ?>
                <input type="file" name="s_ktpa">
                <small class="text-danger"><?=!empty($err_ktpa) ? $err_ktpa : "";?></small>
            <?php } ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">KTP Ibu</label>
        <div class="col-sm-10">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/ktpi/' . $pelaksanaan['s_ktpi'])) { ?>
                    <a href="<?=site_url('uploads/ktpi/' . $pelaksanaan['s_ktpi'])?>" target="_blank" ><?=$pelaksanaan['s_ktpi']?></a>
                <?php } ?>
                <input type="file" name="s_ktpi">
                <small class="text-danger"><?=!empty($err_ktpi) ? $err_ktpi : "";?></small>
            <?php } else { ?>
                <input type="file" name="s_ktpi">
                <small class="text-danger"><?=!empty($err_ktpi) ? $err_ktpi : "";?></small>
            <?php } ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">KIP / KPS</label>
        <div class="col-sm-10">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/kips/' . $pelaksanaan['s_kips'])) { ?>
                    <a href="<?=site_url('uploads/kips/' . $pelaksanaan['s_kips'])?>" target="_blank" ><?=$pelaksanaan['s_kips']?></a>
                <?php } ?>
                <input type="file" name="s_kips">
                <small class="text-danger"><?=!empty($err_kips) ? $err_kips : "";?></small>
            <?php } else { ?>
                <input type="file" name="s_kips">
                <small class="text-danger"><?=!empty($err_kips) ? $err_kips : "";?></small>
            <?php } ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">SK Tidak Mampu</label>
        <div class="col-sm-10">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/sktm/' . $pelaksanaan['s_sktm'])) { ?>
                    <a href="<?=site_url('uploads/sktm/' . $pelaksanaan['s_sktm'])?>" target="_blank" ><?=$pelaksanaan['s_sktm']?></a>
                <?php } ?>
                <input type="file" name="s_sktm">
                <small class="text-danger"><?=!empty($err_sktm) ? $err_sktm : "";?></small>
            <?php } else { ?>
                <input type="file" name="s_sktm">
                <small class="text-danger"><?=!empty($err_sktm) ? $err_sktm : "";?></small>
            <?php } ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Ijazah</label>
        <div class="col-sm-10">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/ijazah/' . $pelaksanaan['s_ijazah'])) { ?>
                    <a href="<?=site_url('uploads/ijazah/' . $pelaksanaan['s_ijazah'])?>" target="_blank" ><?=$pelaksanaan['s_ijazah']?></a>
                <?php } ?>
                <input type="file" name="s_ijazah">
                <small class="text-danger"><?=!empty($err_ijazah) ? $err_ijazah : "";?></small>
            <?php } else { ?>
                <input type="file" name="s_ijazah">
                <small class="text-danger"><?=!empty($err_ijazah) ? $err_ijazah : "";?></small>
            <?php } ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">SKHUN</label>
        <div class="col-sm-10">
            <?php if ($this->uri->segment(2) == 'edit') { ?>
                <?php if (file_exists('./uploads/skhun/' . $pelaksanaan['s_skhun'])) { ?>
                    <a href="<?=site_url('uploads/skhun/' . $pelaksanaan['s_skhun'])?>" target="_blank" ><?=$pelaksanaan['s_skhun']?></a>
                <?php } ?>
                <input type="file" name="s_skhun">
                <small class="text-danger"><?=!empty($err_skhun) ? $err_skhun : "";?></small>
            <?php } else { ?>
                <input type="file" name="s_skhun">
                <small class="text-danger"><?=!empty($err_skhun) ? $err_skhun : "";?></small>
            <?php } ?>
        </div>
    </div> -->
    <hr>

<!--     <h3>Status Kelengkapan Data</h3>
    <p class="help-block">Pilih status kelengkapan data berdasarkan lampiran.</p>
    <hr>
    <div class="form-group">
        <label class="col-sm-2 control-label">Status Data</label>
        <div class="col-sm-3">
            <select id="s_status" name="s_status" class="form-control" required>
                <option></option>
                <option value="Belum Ada Data">Belum Ada Data</option>
                <option value="Kurang">Kurang</option>
                <option value="Lengkap">Lengkap</option>
            </select>
        </div>
    </div> -->

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <!-- <small class="help-block hint">Tombol simpan akan muncul setelah Anda memilih status kelengkapan data.</small> -->
            <!-- <br> -->
            <button type="submit" name="submit" class="btn btn-success" id="submit">Simpan</button>&nbsp;
            <a class="btn btn-default" href="<?=site_url('pelaksanaan')?>">Kembali</a>
        </div>
    </div>
<?=form_close()?>
