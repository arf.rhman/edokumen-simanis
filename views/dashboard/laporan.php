<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>



<h2>Data Laporan</h2>

<div class="table">

    <table id="pelaksanaans" class="display table table-bordered table-hover table-responsive">

        <thead>

            <tr>

                <th width="5%">No</th>

                <th width="12%">Nomor ST</th>

                <th width="12%">Tanggal Upload  ST</th>

                <th width="25%">Judul Laporan</th>

                <th width="12%">Nomor Laporan</th>

                <th width="10%">File / Link</th>

                <th width="10%">Tanggal Upload Laporan</th>

                <th width="10%">Durasi</th>

            </tr>

        </thead>

        <tbody>

        <?php 

        $no=1;

        foreach ($laporan as $key => $value) {?>

           <tr>

            <td><?= $no ?> </td>

            <td><?= $value['nomor_st'] ?></td>

            <td><?= $value['tanggal_st'] ?></td>

            <td><?= $value['judul_laporan']?></td>

            <td><?= $value['nomor_laporan']?></td>

            <td> 

                    <a href="<?= base_url()?>uploads/laporan/<?= $value['file_laporan'] ?>" target="_blank"> <b><?= $value['file_laporan'] ?></b> </a> <br>/


                    <a href="<?= $value['link_gdrive']?>" target="_blank"> <b><?= $value['link_gdrive']?></b> </a> 


            </td>

            <td> <?= $value['tanggal_upload_laporan'] ?> </td>
            <td> <?= ($value['durasi']==0)?1:$value['durasi'] ?> Hari </td>

           <!--  <td> 

            <a class="btn btn-primary btn-sm mb"><span class="glyphicon glyphicon-share" aria-hidden="true"></span></a>

            <a class="btn btn-success btn-sm mb" href="#" target="_blank" title="Download">

            <span class="glyphicon glyphicon-download" aria-hidden="true"></span></a>

             <a class="btn btn-warning btn-sm mb" href="#">

            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>

            </td> -->

        </tr>

        <? $no++;

        }

        ?>

        </tbody>

    </table>

    <script>

        function confirmDialog() {

            return confirm("Apakah Anda yakin akan menghapus data ini?")

        }



    </script>

</div>

