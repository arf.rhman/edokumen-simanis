<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>



<h2><?=$form_title?> <a class="btn btn-primary btn-sm pull-right" href="<?=site_url('mytask')?>"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Kembali</a></h2>

<hr>

<? $jenis = ($pelaksanaan['jenis_st']==1)?'Pembinaan':'Pengawasan'; ?>

<table class="table table-bordered table-striped">

    <tr>

        <th colspan="2" class="text-center"><?= $jenis ?></th>

    </tr>

    <tr>

        <th width="15%"> Nomor ST </th>

        <td> <?= $pelaksanaan['nomor_st'] ?> </td>

    </tr>

    <tr>

        <th> Tanggal ST </th>

        <td> <?= $pelaksanaan['tanggal_st'] ?> </td>

    </tr>

    <tr>

        <th> Dari </th>

        <td> <?= $pelaksanaan['u_fname'] ?> </td>

    </tr>

    <tr>

        <th> Keterangan </th>

        <td> <?= $pelaksanaan['keterangan'] ?> </td>

    </tr>

    <tr>

        <th> File ST </th>

        <td> <a href="<?=site_url('uploads/ST/'.$jenis.'/'.$pelaksanaan['file_st'])?>" target="_blank" ><?=$pelaksanaan['file_st']?></a> </td>

    </tr>

</table>

<?=form_open_multipart($action, 'class="form-horizontal"')?>

<hr>

    <h3>Upload File Pendukung</h3>

      <div class="form-group">

        <label class="col-sm-1 control-label">File 1</label>

        <div class="col-sm-4">

            <?php if ($this->uri->segment(2) == 'edit') { 

                $jenis = ($pelaksanaan['jenis_st']==1)?'Pembinaan':'Pengawasan';

                ?>

                <?php if (file_exists('./uploads/pendukung/'.$jenis.'/'.$det_pelaksanaan[1]['file_pendukung'])) { ?>

                    <a href="<?=site_url('uploads/pendukung/'.$jenis.'/'.$det_pelaksanaan[1]['file_pendukung'])?>" target="_blank" ><?=$det_pelaksanaan[1]['file_pendukung']?></a>

                <?php } ?>

                <input type="file" name="file_pendukung1" required>

                <small class="help-block">Format file yang diperbolehkan *.jpg, *.png, *.pdf, *.doc, *.rar, *.zip, *.ppt, *.pps dan ukuran maksimal 10 MB.</small>

                <!-- <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small> -->

            <?php } else { ?>

                <input type="file" name="file_pendukung1" required>

                <small class="help-block">Format file yang diperbolehkan *.jpg, *.png, *.pdf, *.doc, *.rar, *.zip, *.ppt, *.pps dan ukuran maksimal 10 MB.</small>

                <!-- <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small> -->

            <?php } ?>

        </div>

         <label class="col-sm-1 control-label">Ket. 1</label>

         <div class="col-sm-5">

            <textarea class="form-control" name="keterangan1"><?=isset($det_pelaksanaan[1]['keterangan']) ? $det_pelaksanaan[1]['keterangan'] : set_value('keterangan')?></textarea>

            <!-- <small class="text-danger"><?=form_error('keterangan')?></small> -->

        </div>

    </div>



    <div class="form-group">

        <label class="col-sm-1 control-label">File 2</label>

        <div class="col-sm-4">

            <?php if ($this->uri->segment(2) == 'edit') { 

                $jenis = ($pelaksanaan['jenis_st']==1)?'Pembinaan':'Pengawasan';

                ?>

                <?php if (file_exists('./uploads/pendukung/'.$jenis.'/'.$det_pelaksanaan[2]['file_pendukung'])) { ?>

                    <a href="<?=site_url('uploads/pendukung/'.$jenis.'/'.$det_pelaksanaan[2]['file_pendukung'])?>" target="_blank" ><?=$det_pelaksanaan[2]['file_pendukung']?></a>

                <?php } ?>

                <input type="file" name="file_pendukung2">

                <small class="help-block">Format file yang diperbolehkan *.jpg, *.png, *.pdf, *.doc, *.rar, *.zip, *.ppt, *.pps dan ukuran maksimal 10 MB.</small>

                <!-- <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small> -->

            <?php } else { ?>

                <input type="file" name="file_pendukung2">

                <small class="help-block">Format file yang diperbolehkan *.jpg, *.png, *.pdf, *.doc, *.rar, *.zip, *.ppt, *.pps dan ukuran maksimal 10 MB.</small>

                <!-- <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small> -->

            <?php } ?>

        </div>

         <label class="col-sm-1 control-label">Ket. 2</label>

         <div class="col-sm-5">

            <textarea class="form-control" name="keterangan2"><?=isset($det_pelaksanaan[2]['keterangan']) ? $det_pelaksanaan[2]['keterangan'] : set_value('keterangan')?></textarea>

            <!-- <small class="text-danger"><?=form_error('keterangan')?></small> -->

        </div>

    </div>



    <div class="form-group">

        <label class="col-sm-1 control-label">File 3</label>

        <div class="col-sm-4">

            <?php if ($this->uri->segment(2) == 'edit') { 

                $jenis = ($pelaksanaan['jenis_st']==1)?'Pembinaan':'Pengawasan';

                ?>

                <?php if (file_exists('./uploads/pendukung/'.$jenis.'/'.$det_pelaksanaan[3]['file_pendukung'])) { ?>

                    <a href="<?=site_url('uploads/pendukung/'.$jenis.'/'.$det_pelaksanaan[3]['file_pendukung'])?>" target="_blank" ><?=$det_pelaksanaan[3]['file_pendukung']?></a>

                <?php } ?>

                <input type="file" name="file_pendukung3">

                <small class="help-block">Format file yang diperbolehkan *.jpg, *.png, *.pdf, *.doc, *.rar, *.zip, *.ppt, *.pps dan ukuran maksimal 10 MB.</small>

                <!-- <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small> -->

            <?php } else { ?>

                <input type="file" name="file_pendukung3">

                <small class="help-block">Format file yang diperbolehkan *.jpg, *.png, *.pdf, *.doc, *.rar, *.zip, *.ppt, *.pps dan ukuran maksimal 10 MB.</small>

                <!-- <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small> -->

            <?php } ?>

        </div>

         <label class="col-sm-1 control-label">Ket. 3</label>

         <div class="col-sm-5">

            <textarea class="form-control" name="keterangan3"><?=isset($det_pelaksanaan[3]['keterangan']) ? $det_pelaksanaan[3]['keterangan'] : set_value('keterangan')?></textarea>

            <!-- <small class="text-danger"><?=form_error('keterangan')?></small> -->

        </div>

    </div>



    <div class="form-group">

        <label class="col-sm-1 control-label">File 4</label>

        <div class="col-sm-4">

            <?php if ($this->uri->segment(2) == 'edit') { 

                $jenis = ($pelaksanaan['jenis_st']==1)?'Pembinaan':'Pengawasan';

                ?>

                <?php if (file_exists('./uploads/pendukung/'.$jenis.'/'.$det_pelaksanaan[4]['file_pendukung'])) { ?>

                    <a href="<?=site_url('uploads/pendukung/'.$jenis.'/'.$det_pelaksanaan[4]['file_pendukung'])?>" target="_blank" ><?=$det_pelaksanaan[4]['file_pendukung']?></a>

                <?php } ?>

                <input type="file" name="file_pendukung4">

                <small class="help-block">Format file yang diperbolehkan *.jpg, *.png, *.pdf, *.doc, *.rar, *.zip, *.ppt, *.pps dan ukuran maksimal 10 MB.</small>

                <!-- <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small> -->

            <?php } else { ?>

                <input type="file" name="file_pendukung4">

                <small class="help-block">Format file yang diperbolehkan *.jpg, *.png, *.pdf, *.doc, *.rar, *.zip, *.ppt, *.pps dan ukuran maksimal 10 MB.</small>

                <!-- <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small> -->

            <?php } ?>

        </div>

         <label class="col-sm-1 control-label">Ket. 4</label>

         <div class="col-sm-5">

            <textarea class="form-control" name="keterangan4"><?=isset($det_pelaksanaan[4]['keterangan']) ? $det_pelaksanaan[4]['keterangan'] : set_value('keterangan')?></textarea>

            <!-- <small class="text-danger"><?=form_error('keterangan')?></small> -->

        </div>

    </div>



    <div class="form-group">

        <label class="col-sm-1 control-label">File 5</label>

        <div class="col-sm-4">

            <?php if ($this->uri->segment(2) == 'edit') { 

                $jenis = ($pelaksanaan['jenis_st']==1)?'Pembinaan':'Pengawasan';

                ?>

                <?php if (file_exists('./uploads/pendukung/'.$jenis.'/'.$det_pelaksanaan[5]['file_pendukung'])) { ?>

                    <a href="<?=site_url('uploads/pendukung/'.$jenis.'/'.$det_pelaksanaan[5]['file_pendukung'])?>" target="_blank" ><?=$det_pelaksanaan[5]['file_pendukung']?></a>

                <?php } ?>

                <input type="file" name="file_pendukung5">

                <small class="help-block">Format file yang diperbolehkan *.jpg, *.png, *.pdf, *.doc, *.rar, *.zip, *.ppt, *.pps dan ukuran maksimal 10 MB.</small>

                <!-- <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small> -->

            <?php } else { ?>

                <input type="file" name="file_pendukung5">

                <small class="help-block">Format file yang diperbolehkan *.jpg, *.png, *.pdf, *.doc, *.rar, *.zip, *.ppt, *.pps dan ukuran maksimal 10 MB.</small>

                <!-- <small class="text-danger"><?=!empty($err_foto) ? $err_foto : "";?></small> -->

            <?php } ?>

        </div>

         <label class="col-sm-1 control-label">Ket. 5</label>

         <div class="col-sm-5">

            <textarea class="form-control" name="keterangan5"><?=isset($det_pelaksanaan[5]['keterangan']) ? $det_pelaksanaan[5]['keterangan'] : set_value('keterangan')?></textarea>

            <!-- <small class="text-danger"><?=form_error('keterangan')?></small> -->

        </div>

    </div>

   

    <div class="form-group">

        <label class="col-sm-2 control-label">Keterangan Tambahan</label>

        <div class="col-sm-8">

            <textarea class="form-control" name="keterangan_opd"><?=isset($pelaksanaan['keterangan_opd']) ? $pelaksanaan['keterangan_opd'] : set_value('keterangan_opd')?></textarea>

            <!-- <small class="text-danger"><?=form_error('keterangan')?></small> -->

        </div>

    </div>



    

  

    <hr>

    <div class="form-group">

        <div class="col-sm-offset-2 col-sm-10">

            <!-- <small class="help-block hint">Tombol simpan akan muncul setelah Anda memilih status kelengkapan data.</small> -->

            <!-- <br> -->

            <button type="submit" name="submit" class="btn btn-success" id="submit">Simpan</button>&nbsp;

            <a class="btn btn-default" href="<?=site_url('mytask')?>">Kembali</a>

        </div>

    </div>

<?=form_close()?>

